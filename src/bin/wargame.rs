use std::{env, path};
use std::collections::VecDeque;

use glam::*;

use ggez::event;
use ggez::graphics;
use ggez::graphics::{DrawParam, Drawable};
use ggez::{Context, GameResult};
use ggez::input::mouse::MouseButton;
use ggez::mint::Point2;

use wargame::ui::{Button, Clickable, UiEvent, UiElement};

const GRID_SIZE: (usize, usize) = (20, 20);

const GRID_CELL_SIZE: (usize, usize) = (32, 32);

const SCREEN_SIZE: (f32, f32) = (
    GRID_SIZE.0 as f32 * GRID_CELL_SIZE.0 as f32,
    GRID_SIZE.1 as f32 * GRID_CELL_SIZE.1 as f32,
);

fn pixels_to_idx(pos: &(f32, f32)) -> (usize, usize) {
    let xidx = (pos.0 / (GRID_CELL_SIZE.0 as f32)) as usize;
    let yidx = (pos.1 / (GRID_CELL_SIZE.1 as f32)) as usize;
    (xidx, yidx)
}

struct Unit {
    graphic: graphics::Image,
    pos: (usize, usize),
    hp: i32,
    attack: i32,
    defense: i32,
}

impl Unit {
    fn draw(&self, ctx: &mut Context) -> GameResult {
        graphics::draw(ctx, &self.graphic, graphics::DrawParam::new().dest(Point2 {
                x: GRID_CELL_SIZE.0 as f32 * self.pos.0 as f32,
                y: GRID_CELL_SIZE.1 as f32 * self.pos.1 as f32,
            }),
        )
    }

    fn take_hit(&mut self, attack_other: i32) -> bool {
        println!("Taking hit");
        let mut dmg = attack_other - self.defense;
        if dmg < 0 {
            dmg = 0;
        }
        self.hp -= dmg;
        println!("hp: {}", self.hp);
        if self.hp <= 0 {
            self.hp = 0;
            true
        } else {
            false
        }
    }
}

struct State {
    background: Vec<Vec<graphics::Image>>,
    ui_elements: Vec<Box<dyn UiElement>>,
    ui_event_queue: VecDeque<UiEvent>,
    units: Vec<Unit>,
    hover_idx: (usize, usize),
    selected: Vec<usize>,
}

impl<'a> State {
    fn new(ctx: &mut Context) -> State {
        let mut background = vec![];
        for x in 0..GRID_SIZE.0 {
            let mut temp = vec![];
            for y in 0..GRID_SIZE.1 {
                if (x + y) > 13 {
                    temp.push(graphics::Image::new(ctx, "/water.png").unwrap());
                } else {
                    temp.push(graphics::Image::new(ctx, "/grass.png").unwrap());
                }
            }
            background.push(temp);
        }
        State {
            background,
            ui_elements: vec![],
            ui_event_queue: VecDeque::new(),
            units: vec![],
            hover_idx: (0, 0),
            selected: vec![],
        }
    }

    fn get_selected_units(&'a self) -> Vec<&'a Unit> {
        let mut output = vec![];
        for unit_id in &self.selected {
            let unit = self.units.get(*unit_id).expect("This unit does not exist");
            output.push(unit);
        }
        output
    }

    fn get_unit_at_pos(&self, pos: &(usize, usize)) -> Option<usize> {
        for (id, unit) in self.units.iter().enumerate() {
            if unit.pos == *pos {
                return Some(id);
            }
        }
        None
    }

    fn in_range(&self, unit_id: usize, pos: &(usize, usize)) -> bool {
        let unit = self.units.get(unit_id).expect("This unit does not exist");
        if (unit.pos.0 as i32 - pos.0 as i32).abs() <= 1 && (unit.pos.1 as i32 - pos.1 as i32).abs() <= 1 {
            return true;
        }
        false
    }

    fn take_action(&mut self, unit_id: usize, pos: &(usize, usize)) {
        // Is the target in range?
        if self.in_range(unit_id, pos) {
            if let Some(other_id) = self.get_unit_at_pos(pos) {
                // TODO:Either attack or it's friendly unit
                // For now just always attack!
                if other_id != unit_id {
                    // Stop hitting yourself
                    let unit = self.units.get(unit_id).expect("This unit does not exist");
                    let attack = unit.attack;
                    if self.units[other_id].take_hit(attack) {
                        // If death
                        self.units.remove(other_id);
                        for id in self.selected.iter_mut() {
                            if *id > other_id {
                                *id -= 1;
                            }
                        }
                    }
                }
            } else {
                // If nothing there then we want to move there
                let mut unit = self.units.get_mut(unit_id).expect("this unit does not exist");
                unit.pos = *pos;
            }
        }
    }
}

impl event::EventHandler<ggez::GameError> for State {
    fn update(&mut self, ctx: &mut Context) -> GameResult {

        // Iterate over the ui_event_queue and act out UI events
        for event in self.ui_event_queue.iter() {
            match event {
                UiEvent::EndTurn => println!("Handling End Turn"),
                UiEvent::Restart => println!("Handling Restart"),
                _ => println!("Handling an unknown UI event"),
            }
        }
        self.ui_event_queue.clear();
        Ok(())
    }
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        let mut canvas = graphics::Canvas::from_frame(
            ctx,
            graphics::Color::from([0.1, 0.2, 0.3, 1.0]),
        );

        for x in 0..GRID_SIZE.0 {
            for y in 0..GRID_SIZE.1 {
                canvas.draw(
                    &self.background[y][x],
                    graphics::DrawParam::new().dest(Point2 {
                        x: GRID_CELL_SIZE.0 as f32 * x as f32,
                        y: GRID_CELL_SIZE.1 as f32 * y as f32,
                    }),
                )?;
            }
        }

        // Draw hovered tile
        let hover_box = graphics::Image::new(ctx, "/hover_box.png").unwrap();
        canvas.draw(
            &hover_box,
            graphics::DrawParam::new().dest(Point2 {
                x: GRID_CELL_SIZE.0 as f32 * self.hover_idx.0 as f32,
                y: GRID_CELL_SIZE.1 as f32 * self.hover_idx.1 as f32,
            }),
        )?;

        // Draw selected unit(s)
        let selected_box = graphics::Image::new(ctx, "/selected_box.png").unwrap();
        for id in &self.selected {
            let unit = self.units.get(*id).expect("This unit does not exist");
            canvas.draw(
                &selected_box,
                graphics::DrawParam::new().dest(Point2 {
                    x: GRID_CELL_SIZE.0 as f32 * unit.pos.0 as f32,
                    y: GRID_CELL_SIZE.1 as f32 * unit.pos.1 as f32,
                }),
            )?;
        }

        // Draw our UI
        for element in &self.ui_elements {
            canvas.draw(ctx, &element, DrawParam::default());
        }

        // Draw our units
        for unit in &self.units {
            unit.draw(ctx);
        }


        graphics::present(ctx)?;
        Ok(())
    }

    fn mouse_motion_event(&mut self, ctx: &mut Context, x: f32, y: f32, dx: f32, dy: f32) {
        self.hover_idx = pixels_to_idx(&(x, y));
    }

    fn mouse_button_down_event(&mut self, ctx: &mut Context, button: MouseButton, x: f32, y: f32) {
        let (xidx, yidx) = pixels_to_idx(&(x, y));
        println!("clicked at {}, {}", xidx, yidx);

        match button {
            MouseButton::Left => {
                // First search for hit in UI
                for element in &self.ui_elements {
                    if element.contains(Point2 {x, y}) {
                        // TODO: Handle UI event
                        println!("Clicked a UI element");
                        if let Some(event) = element.event() {
                            self.ui_event_queue.push_back(event);
                        }
                        return;
                    }
                }

                println!("No button clicked");
                // Now check to see if a Unit was clicked
                self.selected.clear();
                for (id, unit) in self.units.iter().enumerate() {
                    if unit.pos == (xidx, yidx) {
                        self.selected.push(id);
                        return;
                    }
                }
            },
            MouseButton::Right => {
                if self.selected.len() > 0 {
                    // If any selected and right click that means take action

                    // For now we just assume only 1 selected unit at time
                    let id = self.selected[0];

                    self.take_action(id, &(xidx, yidx));
                }
            },
            _ => (),
        }
    }
}

pub fn create_default_state(ctx: &mut Context) -> State {
    let mut state = State::new(&mut ctx);
    state.units.push(Unit {
        graphic: graphics::Image::new(&mut ctx, "/white_flag.png").unwrap(),
        pos: (2, 2),
        attack: 1,
        defense: 0,
        hp: 1,
    });
    state.units.push(Unit {
        graphic: graphics::Image::new(&mut ctx, "/red_flag.png").unwrap(),
        pos: (2, 4),
        attack: 1,
        defense: 0,
        hp: 1,
    });
    state.ui_elements.push(Box::new(Button::new(
        "Restart",
        UiEvent::Restart,
        40.0, 40.0,
        120.0, 20.0
    )));
    state.ui_elements.push(Box::new(Button::new(
        "End Turn",
        UiEvent::EndTurn,
        40.0, 60.0,
        120.0, 20.0
    )));

    return state;
}

pub fn main() -> GameResult {
    let resource_dir = if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        path
    } else {
        path::PathBuf::from("./resources")
    };
    let (mut ctx, events_loop) = ggez::ContextBuilder::new("Wargame", "ggez")
        .add_resource_path(resource_dir)
        .window_setup(ggez::conf::WindowSetup::default().title("Wargame"))
        .window_mode(ggez::conf::WindowMode::default().dimensions(SCREEN_SIZE.0, SCREEN_SIZE.1))
        .build()?;
    let state = create_default_state(&mut ctx);
    event::run(ctx, events_loop, state)
}
