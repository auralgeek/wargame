use ggez::{Context, GameResult};
use ggez::context::Has;
use ggez::graphics::{Canvas, Drawable, DrawMode, DrawParam, GraphicsContext, Rect, Text};
use ggez::graphics;
use ggez::mint::Point2;
use crate::ui::{Clickable, UiEvent};

pub struct Button<'a> {
    pub text: Text,
    pub event: UiEvent,
    pub bounds: Rect,
    pub ctx: &'a Context,
}

impl<'a> Button<'a> {
    pub fn new(text: &str, event: UiEvent, posx: f32, posy: f32, dimx: f32, dimy: f32, ctx: &'a Context) -> Button<'a> {
        Button {
            text: Text::new(text),
            event: event,
            bounds: Rect::new(posx, posy, dimx, dimy),
            ctx,
        }
    }
}

impl<'a> Drawable for Button<'a> {
    fn draw(&self, canvas: &mut Canvas, params: impl Into<DrawParam>) {
        // Draw the button background
        let btn = graphics::Mesh::new_rectangle(
            self.ctx,
            DrawMode::fill(),
            self.bounds,
            (127, 127, 127).into(),
        ).expect("failed to create rectangle");
        canvas.draw(&btn, params);

        // Draw the button outline
        let outline = graphics::Mesh::new_rectangle(
            self.ctx,
            DrawMode::stroke(2.0),
            self.bounds,
            (0, 0, 0).into(),
        ).expect("failed to create rectangle");
        canvas.draw(&outline, params);

        // Draw the button text
        self.text.set_bounds([self.bounds.w, self.bounds.h]);
        canvas.draw(&self.text, params);
    }

    fn dimensions(&self, _: &impl Has<GraphicsContext>) -> Option<Rect> {
        Some(self.bounds)
    }
}

impl<'a> Clickable for Button<'a> {
    fn contains(&self, point: Point2<f32>) -> bool {
        self.bounds.contains(point)
    }
    fn event(&self) -> Option<UiEvent> {
        Some(self.event)
    }
}
