pub mod button;
pub use button::Button as Button;

pub mod clickable;
pub use clickable::Clickable as Clickable;

use ggez::graphics::Drawable as Drawable;

pub trait UiElement: Clickable + Drawable {}
impl<D: Clickable + Drawable> UiElement for D {}

#[derive(Debug, Clone, Copy)]
pub enum UiEvent {
    ShowMenu,
    Restart,
    EndTurn,
}
