use ggez::mint::Point2;
use crate::ui::UiEvent;

pub trait Clickable {
    fn contains(&self, point: Point2<f32>) -> bool;
    fn event(&self) -> Option<UiEvent>;
}
